package com.aimao.controller;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 03/29/2019
 */
public class TestController extends Controller {

    private List<Record> getAttrs() {
        List<Record> rets = new ArrayList<Record>();
        Enumeration<String> ens = super.getAttrNames();
        while (ens.hasMoreElements()) {
            String key = ens.nextElement();
            rets.add(new Record().set(key, super.getAttr(key)));
        }
        return rets;
    }

    private Record buildRet(String intro) {
        Record ret = new Record();
        ret.set("intro", intro)
                .set("attrs", this.getAttrs())
                .set("paras", super.getParaMap());
        return ret;
    }
    @ActionKey("/index")
    public void index() {
        set("name", "12aimiao");
        renderJsp("index.jsp");
    }


    @ActionKey("/test1")
    public void test1() {
        renderJson(this.buildRet("zoo list"));
    }



}

