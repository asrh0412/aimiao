package com.aimao.controller;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;

/**
 *
 *
 * @author Roah
 * @since 03/29/2019
 */
public class RestController extends Controller {
    @ActionKey("/test2")
    public void test2() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("12aimiao","12aimiao");
        this.renderJson(jsonObject);
    }
}
