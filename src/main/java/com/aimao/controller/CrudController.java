package com.aimao.controller;

import com.aimao.conf.SQLConfig;
import com.aimao.entity.Student;
import com.aimao.helper.$;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.POST;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Roah
 * @since 03/29/2019
 */
public class CrudController extends Controller {
    static Connection getConnection() throws Exception {
        Connection conn = null;
        ResultSet rs = null;
        Class.forName(SQLConfig.connection);
        conn = DriverManager.getConnection(SQLConfig.url, SQLConfig.username, SQLConfig.passwd);
        return conn;
    }


    static Boolean delete(Integer id) throws Exception {
        Connection conn = getConnection();
        PreparedStatement stmt = null;
        String sql = null;
        String selectSQL = "delete from student where id=?";
        stmt = conn.prepareStatement(selectSQL);
        stmt.setInt(1, id);
        Boolean rs = stmt.execute(sql);
        stmt.close();
        conn.close();
        return rs;
    }

    static Boolean add(Student stu) throws Exception {
        Connection conn = getConnection();
        PreparedStatement stmt = null;
        String sql = "insert into student values (?,?,?)";
        stmt = conn.prepareStatement(sql);
        stmt.setString(1, stu.getStuClass().toString());
        Boolean rs = stmt.execute(sql);
        stmt.close();
        conn.close();
        return rs;
    }

    static List<Student> getList(String sql) throws Exception {
        Connection conn = getConnection();
        PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = null;
        rs = stmt.executeQuery(sql);
        ArrayList<Student> resList = new ArrayList<>();
        while (rs.next()) {
            Student stu = new Student();
            stu.setId(rs.getInt("id"));
            stu.setStuClass(rs.getString("stu_class"));
            stu.setStuId(rs.getInt("stu_id"));
            stu.setStuName(rs.getString("stu_class"));
            resList.add(stu);
        }
        rs.close();
        stmt.close();
        conn.close();
        return resList;
    }

    static Student getOne(String sql) throws Exception {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = null;
        Student stu = new Student();
        rs = stmt.executeQuery(sql);
        rs.next();
        stu.setId(rs.getInt("id"));
        stu.setStuClass(rs.getString("stu_class"));
        stu.setStuId(rs.getInt("stu_id"));
        stu.setStuName(rs.getString("stu_class"));
        rs.close();
        stmt.close();
        conn.close();
        return stu;
    }


    @ActionKey("/standardAdd")
    @Before(POST.class)
    public void standardAdd() {
        JSONObject jsonObject = new JSONObject();
        Connection conn = null;
        try {
            String stuClass = getPara("stu_class");
            String stuName = getPara("stu_name");
            conn = getConnection();
            PreparedStatement stmt = null;
            String sql = "insert into student(stu_id,stu_name,stu_class) value (?,?,?)";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, getParaToInt("stu_id"));
            stmt.setString(2, stuName);
            stmt.setString(3, stuClass);
            Boolean rs = stmt.execute();
            stmt.close();
            conn.close();
            renderJson($.success());
        } catch (Exception e) {
            e.printStackTrace();
            renderJson($.error(e.toString()));
        }

    }

    @ActionKey("/delete")
    public void delete() {
        try {
            delete(getInt());
            renderJson($.success());
        } catch (Exception e) {
            renderJson($.error(e.toString()));
        }
    }

    @ActionKey("/update")
    public void update() {
        Student student = getBean(Student.class);

        try {
            Connection conn = getConnection();
            PreparedStatement stmt = null;
            String stuClass = getPara("stu_class");
            String stuName = getPara("stu_name");
            String sql = "update student set stu_id=?,stu_name=?,stu_class=? where id=?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, getParaToInt("stu_id"));
            stmt.setString(2, stuName);
            stmt.setString(3, stuClass);
            stmt.setInt(4, getParaToInt("id"));
            Boolean rs = stmt.execute();
            renderJson($.success());
        } catch (Exception e) {
            renderJson(e.toString());
        }
    }

    @ActionKey("/selectListByName")
    public void selectList() {
        try {
            String res = getPara("name");
            PreparedStatement p = getConnection().prepareStatement("select * from student where stu_name like  ?");
            p.setString(1, "%"+res+"%");
            ResultSet rs = p.executeQuery();
            ArrayList<Student> students = new ArrayList<>();
            while (rs.next()) {
                Student stu = new Student();
                stu.setId(rs.getInt("id"));
                stu.setStuClass(rs.getString("stu_class"));
                stu.setStuId(rs.getInt("stu_id"));
                stu.setStuName(rs.getString("stu_class"));
                students.add(stu);
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("stuList", students);
            renderJson($.res(jsonObject));
        } catch (Exception e) {
            renderJson($.error(e.toString()));
        }
    }

    @ActionKey("/selectAll")
    public void selectAll() {
        try {
            PreparedStatement p = getConnection().prepareStatement("select * from student");
            ResultSet rs = p.executeQuery();
            ArrayList<Student> students = new ArrayList<>();
            while (rs.next()) {
                Student stu = new Student();
                stu.setId(rs.getInt("id"));
                stu.setStuClass(rs.getString("stu_class"));
                stu.setStuId(rs.getInt("stu_id"));
                stu.setStuName(rs.getString("stu_class"));
                students.add(stu);
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("stuList", students);
            renderJson($.res(jsonObject));
        } catch (Exception e) {
            renderJson($.error(e.toString()));
        }
    }



    @ActionKey("/selectOne")
    public void selectOne() {
        Student student = new Student();
        try {
            String sql = "select * from student where id = "+getParaToInt("id");
            student = getOne(sql);
        } catch (Exception e) {
            renderJson($.error(e.toString()));
        }
        renderJson($.res(student));
    }

}