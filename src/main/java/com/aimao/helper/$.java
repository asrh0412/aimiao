package com.aimao.helper;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 03/31/2019
 */
public class $ {
    private Integer code;
    private String msg;
    private Object data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    public static $ success(){
        $ res = new $();
        res.setCode(1);
        res.setMsg("success");
        return res;
    }
    public static $ error(String e){
        $ res = new $();
        res.setCode(2);
        res.setMsg(e);
        return res;
    }
    public static $ res(Object o){
        $ res = new $();
        res.setCode(1);
        res.setMsg("");
        res.setData(o);
        return res;
    }

}
