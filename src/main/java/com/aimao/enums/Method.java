package com.aimao.enums;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 03/28/2019
 */
public enum Method {
    GET,

    POST,

    PUT,

    PATCH,

    DELETE,

    HEAD,

    OPTIONS;
}
