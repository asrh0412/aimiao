package com.aimao.entity;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 03/29/2019
 */
public class Student {
    private Integer id;
    private Integer stuId;
    private String stuName;
    private String stuClass;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStuId() {
        return stuId;
    }

    public void setStuId(Integer stuId) {
        this.stuId = stuId;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getStuClass() {
        return stuClass;
    }

    public void setStuClass(String stuClass) {
        this.stuClass = stuClass;
    }

    public Student(){
        super();
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", stuId=" + stuId +
                ", stuName=" + stuName +
                ", stuClass=" + stuClass +
                '}';
    }
}
