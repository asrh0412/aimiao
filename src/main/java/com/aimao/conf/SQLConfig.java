package com.aimao.conf;

/**
 * 数据库基础配置
 *
 * @author Roah
 * @since 03/29/2019
 */
public class SQLConfig {
    public static final String url="jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=UTF-8";
    public static final String username="root";
    public static final String passwd="root";
    public static final String connection="com.mysql.jdbc.Driver";

}
