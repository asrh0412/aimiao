package com.aimao.conf;

import com.aimao.controller.CrudController;
import com.aimao.controller.RestController;
import com.aimao.controller.TestController;
import com.jfinal.config.*;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;

/**
 * 初始化配置中心
 *
 * @author Roah
 * @since 03/29/2019
 */
public class MyConfig extends JFinalConfig {

    @Override
    public void configConstant(Constants me) {
        me.setDevMode(true);
        me.setViewType(ViewType.JSP);
    }

    @Override
    public void configRoute(Routes me) {
        me.add("/", TestController.class);
        me.add("/rest", RestController.class);
        me.add("/crud", CrudController.class);
    }

    @Override
    public void configEngine(Engine me) {}
    @Override
    public void configPlugin(Plugins me) {}
    @Override
    public void configInterceptor(Interceptors me) {}
    @Override
    public void configHandler(Handlers me) {}
}
